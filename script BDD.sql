CREATE TABLE challenge
(
id int PRIMARY KEY NOT NULL,
title Text,
content longtext,
due_date timestamp,
user Decimal,
advice longtext
)
