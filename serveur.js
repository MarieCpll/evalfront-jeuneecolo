const mysql = require('mysql');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended : false}))
app.use(bodyParser.json());

require('dotenv').config();

const connection = mysql.createConnection({
	host : process.env.DB_HOST,
	user : process.env.DB_USER,
	password : process.env.DB_PASSWORD,
	database : process.env.DB_DATABASE
})

connection.connect((err)=>{
	if(err){
		console.log(err);
		return;
	}
	console.log("ca marche !");
});

app.use(express.static(`public`))


// app.get('/', function(req, res) {
// 	connection.query("SELECT * FROM user WHERE mail", function(err, result, fields){
// 		if(!!err){
// 			console.log(err);
// 			return
// 		}else{
// 			console.log("SUCESS");
// 			res.send('Hello,' + result[0].mail);
// 		}

// 	});
// 	console.log("OK");
// });

app.get('/', (req, res)=> res.sendFile(__dirname + '/public/accueil.html'))

//query est pour envoyer vers la bdd
app.post('/register', (req, res) => {
	console.log(req.body);
	connection.query(`INSERT INTO user (pseudo, mail, mdp) VALUES 
		'${req.body.pseudo}', '${req.body.mail}', '${req.body.password}';`,(err,result, fields)=> {
			if(err){
				console.log(err);
			}
		});
	//ceci est la reponse pour rediriger vers autre page pour pas que la page tourne dans le vide sans rien renvoyer
	res.sendFile('accueil');
})

app.get('/user/:id', (req, res) => {
	connection.query(`SELECT * FROM user where id = '${req.params.id}';`, (err, result, fields) => {
		if(err){
			console.log(err);
			return
		}

		res.sendFile('/accueil',{result: result[0]})
	})
})

app.listen(process.env.PORT, ()=> console.log(`listen on ${process.env.PORT}`));
